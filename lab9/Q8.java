 public class Q8 {

     int num;
     public static void main(String[] args) {
         Q8 b = foo();
         b.num = 5;

         Q8 c = b;
         c.num = 4;

         Object obj = c;

         Q8 d = foo();
         d.num = b.num;
     }
     private static Q8 foo() {
        return new Q8();
     }
 } 
