 package stack;
 import java.util.Util.ArrayList;
 import java.util.List;
 public class StackImpl<T> implements Stack<T> {

     StackItem<T> top;

     public void push(T item) {
         StackItem<> newTop = new StackItem<>(item,top);
         top = newTop;
     }


     public T pop() {
         T item = top.getItem();

         top = top.getPrevious();
         return item; 
     }
     
     public List<T> toList() {
         ArrayList<T> content = ArrayList<T>();
         StackItem<T> current = top;
         while(current !=null){
             content.add(0,current getItem());
             current = current.getPrevious();
         }
         return content;
     }    
 }
